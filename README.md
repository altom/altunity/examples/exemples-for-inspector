# Exemples for Inspector

Here we have some example that you can connect to with AltUnity Inspector

You need to unzip the project on your platform and the executable


For Mac user who have trouble opening application:
1. Download zip
2. Unzip the zip file
3. Open terminal & go to location of GameName.app
4. Run command in terminal: cd GameName.app
5. Run command in terminal: cd Contents/MacOS
6. Run command in terminal: chmod +x *
7. Go to the app location in finder
8. Keep pressed CTRL key and right click the file and click on Open
9. Click Open in the pop-up “macOS cannot verify the developer of “GameName”. Are you sure you want to open it?”